# どこでもでむMINI（V3以降）のI2C-E2PROMの使い方サンプル

ライブラリを最新にしてお使いください。<br>

バーストライトは実装しておりません。もし行う場合は、32バイトを超えるとアドレスが自動インクリメントされないようなのでご注意ください。<br>

使用しているE2PROMはBR24G64FVM-3A、64K bit(8KB)です。<br>
https://www.rohm.co.jp/products/memory/serial-eeprom/standard/i2c-bus-2-wire/br24g64fvm-3a-product<br>

ご注意<br>
SDカードを使う場合にSD_PWをHIGHにしてSDカードに電源をいれるのですが、<br>
その信号がE2PROMのWriteProtectをHIGHにしてしまうので、E2PROMに書き込みができなくなります。<br>
E2PROMに書き込む場合はSD_PWをLOWにしてください。<br>
また省エネのため、SD_PWをLOWにしておいたほうが良いです。


