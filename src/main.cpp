#include <docodemo.h>
DOCODEMO Dm = DOCODEMO();

TaskHandle_t Handle_main_task;
static void main_task(void *pvParameters)
{
  Dm.begin();
  Dm.LedCtrl(RED_LED, OFF);

  delay(2000); // for USB detection wait

  uint8_t wtdata = random(255);
  uint8_t rddata;

  if (Dm.e2prom_write(0, wtdata))
  {
    SerialDebug.println("Write OK");
    if (Dm.e2prom_read(0, &rddata))
    {
      SerialDebug.println("read OK");
      if (rddata == wtdata)
      {
        SerialDebug.println("verify OK " + String(wtdata, HEX) + " / " + String(rddata, HEX));
      }
      else
      {
        SerialDebug.println("verify error " + String(wtdata, HEX) + " / " + String(rddata, HEX));
      }
    }
    else
    {
      SerialDebug.println("Read Error");
    }
  }
  else
  {
    SerialDebug.println("Write Error");
  }

  while (1)
  {
    Dm.LedCtrl(GREEN_LED, ON);
    vTaskDelay(500);
    Dm.LedCtrl(GREEN_LED, OFF);
    vTaskDelay(500);
  }

  vTaskDelete(NULL);
}

void setup()
{
  SerialDebug.begin(115200);

  vSetErrorSerial(&SerialDebug);

  xTaskCreate(main_task, "main_task", 1024, NULL, tskIDLE_PRIORITY + 1, &Handle_main_task);

  vTaskStartScheduler();

  // error scheduler failed to start
  // should never get here
  while (1)
  {
    SerialDebug.println("Scheduler Failed! \n");
    delay(1000);
  }
}

void loop()
{
  // put your main code here, to run repeatedly:
}